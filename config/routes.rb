Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => 'articles#index'
  get '/about-us', to: 'home#about_us'
  resources :users do
    collection do
      get 'login', to: 'users#login_form'
      post 'login', to: 'users#login'
      post 'login_app', to: 'users#login_app', defaults: {format: :json}
      get 'logout', to: 'users#logout'
    end
  end
  resources :stories
  resources :events
  resources :event_notes
  resources :articles
  resources :downloads

  # mount ActionCable.server => '/cable'
end
