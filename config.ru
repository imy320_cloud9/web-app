# This file is used by Rack-based servers to start the application.
require 'breakpoint'
require 'susy'

require_relative 'config/environment'

run Rails.application
