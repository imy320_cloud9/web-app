class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.integer :user_id
      t.integer :conversation_id
      t.text :body

      t.timestamps
    end
    add_index :messages, [:conversation_id, :user_id]
  end
end
