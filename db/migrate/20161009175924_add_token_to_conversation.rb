class AddTokenToConversation < ActiveRecord::Migration[5.0]
  def change
    add_column :conversations, :token, :string
  end
end
