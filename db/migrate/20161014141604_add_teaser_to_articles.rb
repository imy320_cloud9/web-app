class AddTeaserToArticles < ActiveRecord::Migration[5.0]
  def change
    add_column :articles, :teaser, :text
  end
end
