class CreateDownloads < ActiveRecord::Migration[5.0]
  def change
    create_table :downloads do |t|
      t.integer :user_id
      t.integer :country_id
      t.string :title
      t.text :description

      t.timestamps
    end
    add_index :downloads, [:country_id, :user_id]
  end
end
