class CreateEventNotes < ActiveRecord::Migration[5.0]
  def change
    create_table :event_notes do |t|
      t.integer :user_id
      t.integer :event_id
      t.string :status

      t.timestamps
    end
  end
end
