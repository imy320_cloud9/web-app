class CreateStories < ActiveRecord::Migration[5.0]
  def change
    create_table :stories do |t|
      t.string :name
      t.string :email
      t.boolean :show_gravatar, default: false
      t.text :body
      t.string :title
      t.integer :country_id, index: true

      t.timestamps
    end
  end
end
