class AddsPinToArticles < ActiveRecord::Migration[5.0]
  def change
    add_column :articles, :pin, :boolean, index: true
  end
end
