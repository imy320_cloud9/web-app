class CreateConversations < ActiveRecord::Migration[5.0]
  def change
    create_table :conversations do |t|
      t.integer :cache_message_count
      t.integer :lower_id
      t.integer :higher_id
      t.time :last_message_at

      t.timestamps
    end
    add_index :conversations, [:lower_id, :higher_id, :last_message_at], name: 'conversations_index'
  end
end
