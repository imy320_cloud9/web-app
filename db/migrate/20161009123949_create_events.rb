class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :title
      t.text :body
      t.datetime :start_at
      t.datetime :end_at
      t.integer :user_id, index: true

      t.timestamps
    end
    add_index :events, [:start_at, :end_at]
  end
end
