module ApplicationHelper
  def other_user_in_conversation(me, lower, higher)
    if me == lower
      user_id = higher
    else
      # It means me == higher
      user_id = lower
    end

    User.find(user_id)
  end

  def is_selected(name)
    if controller_name == name
      "Navigation__Item--selected"
    else
      ""
    end
  end

  def article_with_teaser(article)
    # "#{article.teaser}\n\n#{article.body[0.150]}"
    article.teaser
  end
end
