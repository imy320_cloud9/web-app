class ArticlesController < ApplicationController
  load_and_authorize_resource
  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)
    @article.user = current_user
    @article.country_id  = current_user.country_id
    if @article.save
      flash[:notice] = "Article has been created"
      redirect_to @article
    else
      render action: :new
    end
  end

  def show
    # @article = Article.find(params[:id])
  end

  def edit
    # @article = Article.find(params[:id])
    render action: :new
  end

  def update
    # @article = Article.find(params[:id])
    @article.assign_attributes(article_params)
    if @article.save
      flash[:notice] = "Article has been updated"
      redirect_to @article
    else
      render action: :new
    end
  end

  def index
    @articles = Article.all.order(created_at: :DESC)
    respond_to do |format|
      format.json { render json: @articles}
      format.html
    end
  end

  def destroy
    # @article = Article.find(params[:id])
    if @article.destroy
      flash[:notice] = "Article has been deleted"
    else
      flash[:alert] = "Could not delete the article"
    end
    redirect_to @article
  end

  protected
    def article_params
      params.require(:article).permit(:title, :body, :image, :teaser)
    end
end
