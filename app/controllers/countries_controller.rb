class CountriesController < ApplicationController
  load_and_authorize_resource
   def create
     @country = Country.new(country_params)
     @country.save
     flash[:notice] = "Country created"

     redirect_to :root
   end

   def index
     @countries = Country.all.order(:name)
   end

   protected
    def country_params
      params.require(:country).permit(:name)
    end
end
