class EventNotesController < ApplicationController
  def create
    @event_note = EventNote.new(event_note_params)
    @event_note.user_id = current_user.id
    @event_note.save
    redirect_to :back # Hoping this will always work
  end

  def update
    @event_note = EventNote.find(params[:id])
    @event_note.assign_attributes(event_note_params)
    @event_note.save
    render json: @event_note
  end

  def destroy
    @event_note = EventNote.find(params[:id])
    @event_note.destroy
    render json: @event_note
  end

  protected
    def event_note_params
      params.require(:event_note).permit(:status, :event_id)
    end
end
