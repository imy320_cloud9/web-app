class StoriesController < ApplicationController
  load_and_authorize_resource
  def new
    @story = Story.new
  end

  def index
    @stories = Story.all.order(created_at: :DESC)
    respond_to do |format|
      format.json { render json: @stories}
      format.html
    end
  end

  def create
    @story = Story.new(story_params)
    if @story.save
      flash[:notice] = "Story has been posted"
      redirect_to @story
    else
      render action: :new
    end
  end

  def show
    @story = Story.find(params[:id])
  end

  def edit
    @story = Story.find(params[:id])
    render action: :new
  end

  def update
    @story = Story.find(params[:id])
    @story.assign_attributes(story_params)
    if @story.save
      flash[:notice] = "Story has been updated"
      redirect_to @story
    else
      render action: :new
    end
  end

  def destroy
    @story = Story.find(params[:id])
    if @story.destroy
      flash[:notice] = "Story deleted"
    else
      flash[:alert] = "Could not deleted the story"
    end
    redirect_to @story
  end

  protected
    def story_params
      params.require(:story).permit(:name, :email, :show_gravatar, :body, :title, :country_id)
    end
end
