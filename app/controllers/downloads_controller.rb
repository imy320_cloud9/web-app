class DownloadsController < ApplicationController
  load_and_authorize_resource
  def new
    @download = Download.new(country_id: current_user.country_id)
  end

  def create
    @download = Download.new(download_params)
    @download.country_id = current_user.country_id
    @download.user_id = current_user.id

    if @download.save
      flash[:notice] = "You file has been uploaded"
      redirect_to downloads_path
    else
      flash[:alert] = "File not uploaded due to errors"
      render action: :new
    end
  end

  def index
    @downloads = Download.all
    country_id = nil

    if current_user
      country_id = current_user.country_id
    elsif params[:country_id]
      country_id = params[:country_id].to_i
    end

    if country_id
      @downloads = @downloads.where(country_id: country_id)
    end

  end

  def edit
    @download = Download.find(params[:id])
    render action: :new
  end

  def update
    @download = Download.find(params[:id])
    @download.assign_attributes(download_params)

    if @download.save
      flash[:notice] = "You file uploaded has been updated"
      redirect_to downloads_path
    else
      flash[:alert] = "File upload has not been updated due to errors"
      render action: :new
    end
  end

  def destroy
    @download = Download.find(params[:id])
    @download.destroy
    redirect_to downloads_path
  end

  protected
    def download_params
      params.require(:download).permit(:file, :title, :description)
    end
end
