class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception
  protect_from_forgery with: :null_session

  helper_method :role_class, :current_user, :current_user_session

  rescue_from CanCan::AccessDenied do
    render template: 'home/forbidden'
  end

  protected
    def role_class(user)
      if user.admin?
        'User__Admin'
      else
        ''
      end
    end

    # TODO: Replace this method with the real current user
    def current_user_session
        return @current_user_session if defined?(@current_user_session)
        @current_user_session = UserSession.find
    end

    def current_user
      if request.headers['X-User']
        User.find(request.headers['X-User'])
      else
        # User.first
        return @current_user if defined?(@current_user)
        @current_user = current_user_session && current_user_session.user
      end
    end
end
