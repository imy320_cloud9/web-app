class UsersController < ApplicationController
  load_and_authorize_resource

  skip_before_action :verify_authenticity_token, only: [:login_app]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:notice] = "User created"
      redirect_to @user
    else
      render action: :new
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
    render action: :new
  end

  def update
    @user = User.find(params[:id])
    @user.assign_attributes(user_params)
    @user.save
    redirect_to @user
  end

  def login_form
    @user = User.find_by(email: params[:email])
  end

  def login_app 
    @user = User.find_by(email: params[:email])
    if @user && @user.authenticate(params[:password])
      render json: {userId: @user.id, name: @user.name }
    else
      render json: {userId: -1}
    end
  end

  def login
    @user = User.find_by(email: params[:email])
    if @user.persistence_token.nil?
      @user.persistence_token = @user.id
      @user.save!
    end

    if @user && @user.authenticate(params[:password])
      user_session = UserSession.new(@user, params[:remember_me])
      user_session.save
      redirect_to :root
    else
      flash.now[:alert] = "Invalid email and password combination"
      render action: :login_form
    end
  end

  def logout
    current_user_session.destroy
    redirect_to :root
  end

  protected
    def user_params
      params.require(:user).permit(:name, :email, :password, :bio, :role, :position, :avatar, :country_id)
    end
end
