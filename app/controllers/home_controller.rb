class HomeController < ApplicationController
  def about_us
    @users = User.where(role: "admin")
  end

  def forbidden
  end
end
