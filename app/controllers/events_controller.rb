class EventsController < ApplicationController
  load_and_authorize_resource
  def new
    @event = Event.new(start_at: Time.now, end_at: 1.hour.from_now)
  end

  def create
    @event = Event.new(event_params)
    if @event.save
      flash[:notice] = "Event created"
      redirect_to @event
    else
      render action: :new
    end
  end

  def show
    @event = Event.find(params[:id])
  end

  def edit
    @event = Event.find(params[:id])
    render action: :new
  end

  def index
    @events = Event.all.order(:start_at)
    respond_to do |format|
      format.json { render json: @events}
      format.html
    end
  end

  def update
    @event = Event.find(params[:id])
    @event.assign_attributes(event_params)
    if @event.save
      flash[:notice] = "Event has been updated"
      redirect_to @event
    else
      render action: :new
    end
  end

  def destroy
    @event = Event.find(params[:id])
    if @event.destroy
      flash[:notice] = "Event has been deleted"
    else
      flash[:alert] = "Could not delete event"
    end
    redirect_to @event
  end

  protected
    def event_params
      params.require(:event).permit(:title, :start_at, :end_at, :body)
    end
end
