class MessageBroadcastJob < ApplicationJob
  queue_as :default

  def perform(conversation)
    lower_conversations = User.conversations_of(conversation.lower_id)
    higher_conversations = User.conversations_of(conversation.higher_id)
    lower_user_response = ApplicationController.renderer.render file: 'conversations/index', format: :json, locals: {user: conversation.lower, conversations: lower_conversations}
    higher_user_response = ApplicationController.renderer.render file: 'conversations/index', format: :json, locals: {user: conversation.higher, conversations: higher_conversations}

    ActionCable.server.broadcast conversation.lower.token, JSON.parse(lower_user_response)
    ActionCable.server.broadcast conversation.higher.token, JSON.parse(higher_user_response)

    Rails.logger.debug "Message added"
  end
end
