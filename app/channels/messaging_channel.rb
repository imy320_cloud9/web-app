# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class MessagingChannel < ApplicationCable::Channel
  def subscribed
    user = current_user
    # stream_from "some_channel"
    stream_from user.token
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def message(message)
    db_message = {body: message["body"], user_id: current_user.id, to: message["to"]}
    # ActionCable.server.broadcast @user, message # conversations: @user.conversations
    Message.create!(db_message)
    # Rails.logger.debug "Message process for: #{current_user.token}"
  end
end
