App.messaging = App.cable.subscriptions.create "MessagingChannel",
  connected: ->
    # Called when the subscription is ready for use on the server
    console.log self

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    console.log data

  message: (message)->
    @perform 'message', message
