json.id conversation.id
json.user do
  json.partial! other_user_in_conversation(current_user_id, conversation.lower_id, conversation.higher_id)
end
json.last_message_at conversation.last_message_at
json.cache_message_count conversation.cache_message_count
json.messages conversation.messages do |message|
  json.partial! message
end
