json.user do
  json.partial! user
end

json.conversations conversations do |conversation|
  json.partial! conversation, locals: {current_user_id: user.id}
end
