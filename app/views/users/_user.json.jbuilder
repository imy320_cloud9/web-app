json.id user.id
json.name user.name
json.display_picture user.avatar.url(:thumb)
