class Country < ApplicationRecord
  has_many :users
  has_many :events
  has_many :stories
  has_many :articles
  has_many :downloads
end
