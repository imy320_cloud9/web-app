class Message < ApplicationRecord
  attr_accessor :to
  belongs_to :user
  belongs_to :conversation, touch: true

  after_create_commit { 
    conversation = self.conversation
    number_of_messages = Message.where(conversation_id: conversation.id).count
    conversation.assign_attributes(cache_message_count: number_of_messages, last_message_at: self.created_at)
    conversation.save!
    MessageBroadcastJob.perform_later conversation
  }

  before_validation :set_conversation
  protected
    def set_conversation
      conversation = User.find(self.user_id).conversation_with(@to)
      self.conversation_id = conversation.id
    end
end
