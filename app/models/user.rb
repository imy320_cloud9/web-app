class User < ApplicationRecord
  has_secure_password
  belongs_to :country
  has_many :downloads

  before_validation :reset_persistence_token, :if => :reset_persistence_token?

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "128x128#" }, default_url: "/system/images/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  def admin?
    self.role == 'admin'
  end

  def conversation_with(other_id)
    if self.id > other_id
      lower_id, higher_id = other_id, self.id
    else
      lower_id, higher_id = self.id, other_id
    end

    Conversation.find_or_create_by!(lower_id: lower_id, higher_id: higher_id)
  end

  def self.conversations_of(user_id)
    Conversation.includes(:messages).where('lower_id = ? OR higher_id = ?', user_id, user_id)
  end

  def token
    Digest::MD5.hexdigest(self.id.to_s)
  end

  protected
    def reset_persistence_token?
      self.persistence_token.nil?
    end

    def reset_persistence_token
      self.persistence_token = self.id if self.id.present?
    end
end
