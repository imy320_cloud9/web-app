class Article < ApplicationRecord
  belongs_to :user
  belongs_to :country
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/system/images/no-image.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  before_validation :shorten_teaser

  protected
    def shorten_teaser
      self.teaser = self.teaser.to_s[0..301]
    end
end
