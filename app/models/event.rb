class Event < ApplicationRecord
  belongs_to :user
  belongs_to :country
  has_many :responses, class_name: 'EventNote', dependent: :destroy
end
