class Download < ApplicationRecord
  belongs_to :user
  has_attached_file :file
  validates_attachment_content_type :file, content_type: ["application/zip", "application/x-zipa", "application/pdf"]
end
